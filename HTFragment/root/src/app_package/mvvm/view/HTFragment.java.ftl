package ${packageName}.mvvm.view.fragment;

import ${superClassFqcn};
import android.os.Bundle;
import androidx.annotation.Nullable;
import ${packageName}.R;
<#if generateViewModel>
import ${packageName}.mvvm.viewmodel.${shortName}ViewModel;
</#if>


/**
 * ${packageName}
 *
 * @author :
 * @describe :
 * @date : ${.now?string("yyyy/MM/dd")}
 */
public class ${fragmentClass} extends HTMVVMFragment<${shortName}ViewModel,${moduleName}${shortName}FragmentBinding> {



    public static ${fragmentClass} newInstance(Object[] args) {
     return new ${fragmentClass}();
    }

    @Override
    public int getLayoutId() {
        return R.layout.${layoutName};
    }


    @Override
    public void initData(@Nullable Bundle savedInstanceState) {


    }
}
