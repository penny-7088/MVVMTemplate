package ${packageName}.mvvm.viewmodel;

import ${superClassFqcn};
import androidx.annotation.NonNull;
import javax.inject.Inject;
import android.app.Application;
import ${packageName}.mvvm.model.${modelName};
/**
 * ${packageName}
 *
 * @author :
 * @describe :
 * @date : ${.now?string("yyyy/MM/dd")}
 */
public class ${viewmodelName} extends BaseViewModel<${modelName}> {

    @Inject
    public ${viewmodelName}(@NonNull Application application,${modelName} model){
      super(application,model);
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

}
