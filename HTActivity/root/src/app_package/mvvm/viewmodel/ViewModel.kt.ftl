package ${escapeKotlinIdentifiers(packageName)}

import ${superClassFqcn}
import ${superClassFqcn};
import androidx.annotation.NonNull;
import javax.inject.Inject;

class ${viewmodelName} : BaseViewModel<${modelName}>() {

  @Inject
  constructor(application:Application,${modelName} model){
    super(application,model)
  }

}
