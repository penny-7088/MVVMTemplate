package ${packageName}.mvvm.view.activity;

import ${superClassFqcn};
import android.os.Bundle;
<#if generateViewModel>
import ${packageName}.mvvm.viewmodel.${shortName}ViewModel;
</#if>


/**
 * ${packageName}
 *
 * @author :
 * @describe :
 * @date : ${.now?string("yyyy/MM/dd")}
 */
public class ${activityClass} extends HTMVVMActivity<${shortName}ViewModel,${moduleName}${shortName}ActivityBinding> {


    @Override
    public int getLayoutId() {
        return R.layout.${layoutName};
    }


    @Override
    public void initData(@Nullable Bundle savedInstanceState) {


    }
}
