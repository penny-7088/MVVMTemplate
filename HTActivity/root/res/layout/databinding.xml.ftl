<?xml version="1.0" encoding="utf-8"?>
<layout xmlns:android="http://schemas.android.com/apk/res/android"
        xmlns:tools="http://schemas.android.com/tools">

    <data>
        <#if generateViewModel>
    	<variable
                name="vm"
                type="${packageName}.mvvm.viewmodel.${viewmodelName}"/>
        </#if>
    </data>

    <LinearLayout
        android:layout_width="match_parent"
        android:layout_height="match_parent"
        android:orientation="vertical">

        <ViewStub
            android:id="@+id/base_app_bar_layout"
            android:layout_width="match_parent"
            android:layout_height="56dp"
            android:layout="@layout/common_toolbar" />

    </LinearLayout>
</layout>
