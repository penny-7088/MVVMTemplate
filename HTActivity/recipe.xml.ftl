<?xml version="1.0"?>
<#import "root://activities/common/kotlin_macros.ftl" as kt>
<recipe>

	<instantiate from="root/res/layout/databinding.xml.ftl"
                   to="${escapeXmlAttribute(resOut)}/layout/${layoutName}.xml" />

    <#if generateViewModel>
    <instantiate from="root/src/app_package/mvvm/viewmodel/ViewModel.${ktOrJavaExt}.ftl"
                   to="${escapeXmlAttribute(srcOut)}/mvvm/viewmodel/${shortName}ViewModel.${ktOrJavaExt}" />
    </#if>

    <instantiate from="root/src/app_package/mvvm/view/HTActivity.${ktOrJavaExt}.ftl"
                   to="${escapeXmlAttribute(srcOut)}/mvvm/view/activity/${activityClass}.${ktOrJavaExt}" />

    <instantiate from="root/src/app_package/mvvm/model/Model.${ktOrJavaExt}.ftl"
                  to="${escapeXmlAttribute(srcOut)}/mvvm/model/${shortName}Model.${ktOrJavaExt}" />

    <open file="${escapeXmlAttribute(srcOut)}/${activityClass}.${ktOrJavaExt}" />
</recipe>
