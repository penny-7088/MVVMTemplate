<?xml version="1.0"?>
<#import "root://activities/common/kotlin_macros.ftl" as kt>
<recipe>

	<instantiate from="root/res/layout/databinding.xml.ftl"
                   to="${escapeXmlAttribute(resOut)}/layout/${activityLayoutName}.xml" />

	 <instantiate from="root/res/layout/databinding.xml.ftl"
                    to="${escapeXmlAttribute(resOut)}/layout/${fragmentLayoutName}.xml" />

    <#if generateViewModel>
    <instantiate from="root/src/app_package/mvvm/viewmodel/ViewModel.${ktOrJavaExt}.ftl"
                   to="${escapeXmlAttribute(srcOut)}/mvvm/viewmodel/${shortName}ViewModel.${ktOrJavaExt}" />
    </#if>
##activty
		<instantiate from="root/src/app_package/mvvm/view/activity/HTActivity.${ktOrJavaExt}.ftl"
									 to="${escapeXmlAttribute(srcOut)}/mvvm/view/activity/${activityClass}.${ktOrJavaExt}" />
#fragment
		<instantiate from="root/src/app_package/mvvm/view/fragment/HTFragment.${ktOrJavaExt}.ftl"
	 								 to="${escapeXmlAttribute(srcOut)}/mvvm/view/fragment/${fragmentClass}.${ktOrJavaExt}" />

#model
		<instantiate from="root/src/app_package/mvvm/model/Model.${ktOrJavaExt}.ftl"
									to="${escapeXmlAttribute(srcOut)}/mvvm/model/${shortName}Model.${ktOrJavaExt}" />
#di
		<instantiate from="root/src/app_package/di/component/DI.${ktOrJavaExt}.ftl"
							    to="${escapeXmlAttribute(srcOut)}/di/component/${moduleName}Component.${ktOrJavaExt}" />

		<instantiate from="root/src/app_package/di/module/module.${ktOrJavaExt}.ftl"
								  to="${escapeXmlAttribute(srcOut)}/di/module/${moduleName}Module.${ktOrJavaExt}" />

		<instantiate from="root/src/app_package/di/module/activityModule.${ktOrJavaExt}.ftl"
							    to="${escapeXmlAttribute(srcOut)}/di/module/${shortName}ActivityModule.${ktOrJavaExt}" />

		<instantiate from="root/src/app_package/di/module/fragmentModule.${ktOrJavaExt}.ftl"
							    to="${escapeXmlAttribute(srcOut)}/di/module/${shortName}FragmentModule.${ktOrJavaExt}" />

		<instantiate from="root/src/app_package/di/module/viewModelModule.${ktOrJavaExt}.ftl"
							    to="${escapeXmlAttribute(srcOut)}/di/module/${shortName}ViewModelModule.${ktOrJavaExt}" />

#api
		<instantiate from="root/src/app_package/api/Service.${ktOrJavaExt}.ftl"
							    to="${escapeXmlAttribute(srcOut)}/api/${shortName}Service.${ktOrJavaExt}" />


    <open file="${escapeXmlAttribute(srcOut)}/${fragmentClass}.${ktOrJavaExt}" />
</recipe>
