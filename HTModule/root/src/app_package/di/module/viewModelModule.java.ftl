package ${packageName}.di.module;

import ${superClassFqcn};
import dagger.Module;

/**
 * ${packageName}
 *
 * @author :
 * @describe :
 * @date : ${.now?string("yyyy/MM/dd")}
 */
 @Module
 public abstract class ${shortName}ViewModelModule {


 }
