package ${escapeKotlinIdentifiers(packageName)}

import ${superClassFqcn}
import ${superClassFqcn};
import androidx.annotation.NonNull;
import javax.inject.Inject;

class ${modelName} : BaseModel() {

  @Inject
  constructor(dataRepository:IDataRepository){
    super(dataRepository)
  }

}
