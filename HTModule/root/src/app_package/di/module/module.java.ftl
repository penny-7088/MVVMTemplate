package ${packageName}.di.module;

import ${superClassFqcn};


/**
 * ${packageName}
 *
 * @author :
 * @describe :
 * @date : ${.now?string("yyyy/MM/dd")}
 */
 @Module(includes = {ViewModelFactoryModule.class,
                     ${shortName}ActivityModule.class,
                     ${shortName}FragmentModule.class,
                     ${shortName}ViewModelModule.class})
 public class ${moduleName}Module {

 }
