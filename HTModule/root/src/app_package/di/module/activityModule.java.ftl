package ${packageName}.di.module;

import ${superClassFqcn};
import dagger.Module;


/**
 * ${packageName}
 *
 * @author :
 * @describe :
 * @date : ${.now?string("yyyy/MM/dd")}
 */
 @Module(subcomponents = BaseActivitySubComponent.class)
 public abstract class ${shortName}ActivityModule {

 }
