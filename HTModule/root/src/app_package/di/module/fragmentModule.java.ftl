package ${packageName}.di.module;

import ${superClassFqcn};
import dagger.Module;

/**
 * ${packageName}
 *
 * @author :
 * @describe :
 * @date : ${.now?string("yyyy/MM/dd")}
 */
 @Module(subcomponents = BaseFragmentSubComponent.class)
 public abstract class ${shortName}FragmentModule {
 }
