package ${packageName}.di.component;

import ${superClassFqcn};


/**
 * ${packageName}
 *
 * @author :
 * @describe :
 * @date : ${.now?string("yyyy/MM/dd")}
 */
@Component(dependencies = AppComponent.class, modules = ${moduleName}Module.class)
public interface ${moduleName}Component extends ApplicationInject {


}
