package ${packageName}.mvvm.model;

import ${superClassFqcn};
import javax.inject.Inject;

/**
 * ${packageName}
 *
 * @author :
 * @describe :
 * @date : ${.now?string("yyyy/MM/dd")}
 */
public class ${modelName} extends BaseModel {

    @Inject
    public ${modelName}(IDataRepository dataRepository){
      super(dataRepository);
    }



}
