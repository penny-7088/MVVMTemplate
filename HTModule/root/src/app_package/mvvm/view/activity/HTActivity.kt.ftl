package ${escapeKotlinIdentifiers(packageName)}

import ${superClassFqcn}
import android.os.Bundle
import org.koin.android.viewmodel.ext.android.viewModel
import ${packageName}.mvvm.viewmodel.${shortName}ViewModel
import ${packageName}.mvvm.model.${shortName}Model

/**
 * ${packageName}
 *
 * @author :
 * @describe :
 * @date : ${.now?string("yyyy/MM/dd")}
 */
class ${activityClass} : HTMVVMActivity<${shortName}ViewModel,${moduleName}${shortName}Binding>() {


    override fun getLayoutId(): Int = R.layout.${activityLayoutName}


    override fun initData(savedInstanceState:Bundle){}
}
